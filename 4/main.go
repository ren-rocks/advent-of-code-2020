package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

func main() {
	var err error
	var line string
	var counter int

	foundFields := map[string]string{}
	scanner := getInputScanner("./input")

	foundEOL := func(foundFields map[string]string) {
		delete(foundFields, "cid")

		if len(foundFields) < 7 {
			return
		}

		byr, _ := strconv.Atoi(foundFields["byr"])
		iyr, _ := strconv.Atoi(foundFields["iyr"])
		eyr, _ := strconv.Atoi(foundFields["eyr"])

		if byr < 1920 || byr > 2002 {
			fmt.Println("BYR", 1920, "<", byr, "<", 2002)
			return
		}

		if iyr < 2010 || iyr > 2020 {
			fmt.Println("IYR", 2010, "<", iyr, "<", 2020)
			return
		}

		if eyr < 2020 || eyr > 2030 {
			fmt.Println("EYR", 2020, "<", eyr, "<", 2030)
			return
		}

		var metric string
		var hgtMin, hgtMax int
		switch true {
		case strings.HasSuffix(foundFields["hgt"], "cm"):
			metric = "cm"
			hgtMin = 150
			hgtMax = 193
			break
		case strings.HasSuffix(foundFields["hgt"], "in"):
			metric = "in"
			hgtMin = 59
			hgtMax = 76
			break
		default:
			fmt.Println("HGT", foundFields["hgt"])
			return
		}

		hgt := strings.Replace(foundFields["hgt"], metric, "", -1)
		hgtInt, _ := strconv.Atoi(hgt)
		if hgtInt < hgtMin || hgtInt > hgtMax {
			fmt.Println("HGT", hgtMin, "<", hgtInt, "<", hgtMax, metric)
			return
		}

		foundHCL, _ := regexp.MatchString(`(?m)\#[a-f0-9]{6}`, foundFields["hcl"])
		if !foundHCL {
			fmt.Println("HCL", "`(?m)\\#[a-f0-9]{6}`", foundFields["hcl"])
			return
		}

		validColors := map[string]bool{
			"amb": true,
			"blu": true,
			"brn": true,
			"gry": true,
			"grn": true,
			"hzl": true,
			"oth": true,
		}

		_, foundECL := validColors[foundFields["ecl"]]
		if !foundECL {
			fmt.Println("ECL", foundFields["ecl"])
			return
		}

		foundPID, _ := regexp.MatchString(`(?m)[0-9]{9}`, foundFields["pid"])
		if !foundPID {
			fmt.Println("PID", "(?m)[0-9]{9}", foundFields["pid"])
			return
		}

		counter++
	}

	for scanner.Scan() {
		line = scanner.Text()

		if line == "" {
			foundEOL(foundFields)
			foundFields = map[string]string{}
			continue
		}

		fields := strings.Split(line, " ")
		for _, val := range fields {
			content := strings.Split(val, ":")
			foundFields[content[0]] = content[1]
		}
	}

	// foundEOL(foundFields)

	fmt.Println(counter)

	if err = scanner.Err(); err != nil {
		fmt.Println("error", err)
		os.Exit(1)
	}
}

func getInputScanner(input string) *bufio.Scanner {
	var err error
	var f *os.File

	if f, err = os.Open(input); err != nil {
		fmt.Println("error", err)
		os.Exit(1)
	}

	return bufio.NewScanner(f)
}
