package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
)

func main() {
	var err error
	var counter, indexA, indexB int

	var re = regexp.MustCompile(`(\d+)-(\d+)\s([a-z])\:\s([a-z]+)$`)
	scanner := getInputScanner()

	for scanner.Scan() {
		line := scanner.Text()
		matches := re.FindAllStringSubmatch(line, -1)[0]
		letter := matches[3]
		password := matches[4]

		if indexA, err = strconv.Atoi(matches[1]); err != nil {
			fmt.Println("error", err)
			os.Exit(1)
		}

		if indexB, err = strconv.Atoi(matches[2]); err != nil {
			fmt.Println("error", err)
			os.Exit(1)
		}

		indexA--
		indexB--

		if password[indexA] == letter[0] && password[indexB] == letter[0] {
			continue
		}

		if password[indexA] != letter[0] && password[indexB] != letter[0] {
			continue
		}

		counter++
	}

	fmt.Println("count:", counter)

	if err = scanner.Err(); err != nil {
		fmt.Println("error", err)
		os.Exit(1)
	}
}

func getInputScanner() *bufio.Scanner {
	var err error
	var f *os.File

	if f, err = os.Open("./input"); err != nil {
		fmt.Println("error", err)
		os.Exit(1)
	}

	return bufio.NewScanner(f)
}
