package main

import (
	"bufio"
	"fmt"
	"os"
)

type slope struct {
	size  int
	down  int
	index int
	total int
}

func main() {
	var err error
	var total, lineIndex int

	slopes := []slope{
		{size: 1},
		{size: 3},
		{size: 5},
		{size: 7},
		{size: 1, down: 2},
	}

	scanner := getInputScanner("./input")

	for scanner.Scan() {
		line := scanner.Text()
		lineSize := len(line)

		for i := range slopes {
			if slopes[i].down != 0 && lineIndex%slopes[i].down != 0 {
				continue
			}

			for slopes[i].index >= lineSize {
				slopes[i].index -= lineSize
			}

			if string(line[slopes[i].index]) == "#" {
				slopes[i].total++
			}

			slopes[i].index += slopes[i].size
		}

		lineIndex++
	}

	total = slopes[0].total
	for _, slope := range slopes[1:] {
		total *= slope.total
	}

	fmt.Println(total)

	if err = scanner.Err(); err != nil {
		fmt.Println("error", err)
		os.Exit(1)
	}
}

func getInputScanner(input string) *bufio.Scanner {
	var err error
	var f *os.File

	if f, err = os.Open(input); err != nil {
		fmt.Println("error", err)
		os.Exit(1)
	}

	return bufio.NewScanner(f)
}
