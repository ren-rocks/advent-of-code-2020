package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	var err error
	var f *os.File
	var num int

	if f, err = os.Open("./input"); err != nil {
		fmt.Println("error", err)
		os.Exit(1)
	}

	scanner := bufio.NewScanner(f)
	m := make(map[int]bool)
	total := 2020
	for scanner.Scan() {
		if num, err = strconv.Atoi(scanner.Text()); err != nil {
			continue
		}

		_, ok := m[total-num]

		if ok {
			fmt.Println(num, total-num, num*(total-num))
			os.Exit(0)
		}

		m[num] = true
	}

	if err = scanner.Err(); err != nil {
		fmt.Println("error", err)
		os.Exit(1)
	}
}
