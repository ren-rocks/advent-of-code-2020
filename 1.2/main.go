package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	var err error
	var f *os.File
	var first, second, third int
	total := 2020

	if f, err = os.Open("./input"); err != nil {
		fmt.Println("error", err)
		os.Exit(1)
	}

	scanner := bufio.NewScanner(f)
	n := make(map[int]bool)

	for scanner.Scan() {
		if first, err = strconv.Atoi(scanner.Text()); err != nil {
			continue
		}

		n[first] = true

		for second = range n {
			third = total - first - second
			_, found := n[third]

			if found {
				fmt.Println(first, second, third, first+second+third, first*second*third)
				os.Exit(0)
			}
		}
	}

	if err = scanner.Err(); err != nil {
		fmt.Println("error", err)
		os.Exit(1)
	}
}
